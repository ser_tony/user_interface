
var ENTER_KEY = 13;

$(document).ready(function () {

	//инициализация приложения
	Backbone.history.start();

	//конфигурации - состояния сортировки, фильтра (Первоначально загружаются из localStorage/indexDB  или создаются по умолчанию)
	var sort = new App.Sort(),
		filter = new App.Filter();

	window.App.States = {
		sort: sort,
		filter: filter
	};

	window.App.currentUser = new App.User();
	window.App.userCollection = new App.UserCollection();


	//слушаем модель сортировки
	window.App.userCollection.listenTo(window.App.States.sort, 'change', _.debounce(function () {
		window.App.userCollection.sorting(window.App.States.sort);
	}));
	

	//инициализация вьюшек 
	var sortPanel = new App.SortPanelView({model: window.App.States.sort }); 
	var userListView = new App.UserListView({
							collection: window.App.userCollection,
							model: window.App.currentUser
						});
	var paginatePanel = new App.PaginatorView();
	var fullInfo = new App.FullInfoView({model: window.App.currentUser });
	
	var filterPanel = new App.FilterPanel({model: window.App.States.filter});	

	//подгрузить AJAXом данные в коллекцию
	window.App.userCollection.fetch({
		resetQueue: true
	});	


	sort.fetch({
		success: function (d) {
			if (typeof d.attributes[0] != 'undefined') {
				d.set('id', d.attributes[0].id);
				d.set('direction', d.attributes[0].direction);
				d.set('field', d.attributes[0].field);
			}
		}
	});

	filter.fetchLocale();

	window.App.currentUser.fetchLocale();

	sortPanel.render();
	fullInfo.render();
	filterPanel.render();
	paginatePanel.render();


});

