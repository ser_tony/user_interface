	
App.UserListView =  Thorax.View.extend({
	 	name: 'user_list_t',
	 	el: '#user_list',
	 	events: {
	 		'click .t_row': 'selectUser'
	 	},

	 	//по каким критериям происходит фильтрование списка
	 	criteria: null,

	 	initialize: function () {
	 		//слушаем изменения коллекции юзеров  - при добавлении, удалении перерисовка
	 		this.listenTo(this.collection, 'all', _.debounce(function () {
	 			this.render();
	 			this.selectInDOM();
	 		}));

	 		this.listenTo(this.model, 'change', function () {
	 			this.selectInDOM();	
	 		});

	 		this.listenTo(App.States.filter, 'change:criteria', _.debounce( function (d) {
	 			//изменился критерий - о чем оповещает модель фильтра
	 			this.criteria = (!_.isEmpty(d.get('criteria'))) ? d.get('criteria') : null;
	 			this.updateFilter();
	 			var len = this.$el.find('.t_row:visible').length;
	 			$('.user_number').text(len);
	 		}));
	 	},


	 	itemFilter: function (model, index) {
	 		/**  фильтрующий метод. в список попадут модели только вернувшие true - удовлетворившие требованиям */

	 		var me = this;

	 		if (this.criteria == null)	return true;

	 		//console.log(model.get('uid'));
	 		var success = 0,
	 			len = this.criteria.length;	
	 		for (var i = 0; i< len; i++) {
	 			var itemCr = this.criteria[i];

	 			/** берем по порядку основные поля пользователя (которые присутсвуют в списке)
	 				и проводим фильтрацию - сравнение с критерием
	 			*/

	 			var available = ['uid', 'fName', 'lName', 'mName', 'price'],
	 				fres = _.find(available, function(str) {
	 							if ((""+model.get(str)).toUpperCase().indexOf(itemCr) != -1 )	
	 								return true;
	 						});
	 				if (typeof fres != 'udefined' && fres ) {
	 					success++;
	 				}			 
		 			var fromModel = moment(model.get('date')).format('D MMMM YYYY').toUpperCase();
					if (fromModel.indexOf(itemCr) != -1 ) {
						success++;
					}
	 		}

	 		return (success >= len);
		},

	 	selectInDOM: function() {
	 		this.$el.find('.t_row')
	 			.removeClass('selected')
	 			.filter('[data-id="'+ App.currentUser.id +'"]')
	 			.addClass('selected');
	 	},

	 	selectUser: function (evt) {
	 		var target = $(evt.target);	
	 		App.currentUser.set(target.model().attributes);
	 		App.currentUser.saveLocale();
	 	}

	 });

App.FullInfoView = Thorax.View.extend({
	name: 'full_info_view',
	el: '#full_info',

	initialize: function () {
		this.listenTo(this.model, 'change', _.debounce(function () {
			this.render();	
		}));
	}
});

App.SortPanelView = Thorax.View.extend({
	name: 'table_head_view',
	el: '#table_head',

	initialize: function () {
		this.listenTo(this.model, 'change', _.debounce(function () {
			this.$el.find('.t_title')
				.removeClass('selected')
				.filter('[data-field="'+  this.model.get('field') +'"]')
				.addClass('selected');
		}));
	},

	events: {
		'click .t_title': 'setSort'
	},

	setSort: function (evt) {
		var target = $(evt.target),
		f = target.data('field');
		this.model.setField(f);
	}

});

App.FilterPanel = Thorax.View.extend({
	name: 'filter_view',
	el: '#filter_str',
	events : {
		'keyup input' : 'pressKey'
	},

	pressKey: function(evt) {
		if (evt.which != ENTER_KEY)
			return false;
		var text = this.$el.find('input').val();
		if (text.length < 3) {
			this.model.clearText();
			return;
		}
		this.model.updateText(text);
	}
});

App.PaginatorView = Thorax.View.extend({
	name: 'paginator_view',
	el: '#paginator_part',
	currentPage: 1,
	events: {
		'click a': 'pageChange'
	},

	pageChange: function (evt) {
		var target = $(evt.target);
		this.currentPage = target.data('id');
		this.$el.find('li').removeClass('active')
			.find('a[data-id="'+ this.currentPage +'"]')
			.parents('li').addClass('active');
	}
});
