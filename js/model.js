
App.LocalBaseModel = Thorax.Model.extend({
	/** Базовый класс под реализацию локального хранилища localStorage **/

	locationKey: '',
	
	isStorage: function () {
		return (localStorage && typeof localStorage != 'undefined')? true: false;
	},

	saveLocale: function () {
		/** кладем в хранилище **/
		if (this.isStorage()) {
			try {
				localStorage.setItem(this.locationKey, JSON.stringify (this.attributes));
			}
			catch (e) {
				console.log(e);
			}
		} 
	},

	fetchLocale: function () {
		/** берем из хранилища */
		if (this.isStorage()) {
			try {
				var obj = localStorage.getItem(this.locationKey);
				obj = JSON.parse(obj);
				this.set(obj);
			}
			catch (e) {
				console.log(e);
			}				
		}
	}
});


App.User = App.LocalBaseModel.extend({
	locationKey: 'current_user',
	idAttribute: "uid",

	//поля юзера из data.json
	defaults: {
		about: "",
		fName: " - ",
		lName: " - ",
		mName: " - ",
		photo: "not.png",
		price: 0.00,
		date: " - ",
		uid: ""
	}
});


App.Sort = Thorax.Model.extend({
	/** Модель хранит и обрабатывает свойства сортировки списка **/

	defaults: {
		direction: 'ASC', //or DESC
		field: ''
	},
	localStorage: new Store('sort-state'),

	isEnable: function () {
		return this.get('field') !== '';
	},

	setField: function (f) {
		if (f == '') return;
		//установка направления сортировки
		if (f == this.get('field')) {
			if (this.get('direction') == 'ASC') {
				this.set('direction', 'DESC');
			}
			else {
				this.set('direction', 'ASC');	
			}
		}
		else {
			this.set('field', f);	
		}
		this.save();
	}
});

App.Filter = App.LocalBaseModel.extend({
	/** Модель под текстовый фильтр */

	locationKey: 'filter',

	defaults: {
		criteria: [],
		filter_text: ''
	},

	clearText: function() {
		/** clear filter  */
		this.set('filter_text', '');
		this.set('criteria', []);
		this.saveLocale();
	},

	updateText: function(f_str) {
		/** split valid string to parts  
		and afterwards trigger Filter at UserCollection**/
		var parts = _.reject(f_str.toUpperCase().split(' ') , function(s) {return s.trim()== '';});
		this.set('criteria', parts);
		this.set('filter_text', f_str);
		this.saveLocale();
	}
});
