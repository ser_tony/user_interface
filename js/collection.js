
App.UserCollection = Thorax.Collection.extend({
	model: App.User,
	url: '/data/data.json',

	initialize: function () {
		// this.listenTo(this.sortInfo, 'change', _.debounce(function () {
		// 		debugger;
		// 		this.sorting();
		// 	}));
	},

	filtering: function(criteria_list) {

	},

	sorting: function(sort) {
		this.comparator = this.criteriaComparator(sort.get('field'), sort.get('direction'));
		return this.sort();
	},

	criteriaComparator: function (field, direct) {
		//выбор направления сортировки и поля 
		return function (a, b) {
			var aSort = a.get(field),
				bSort = b.get(field);
				if (direct == 'ASC') {
					if (aSort < bSort) {
						return -1;
					} 
					if (aSort > bSort) {
						return 1;
					}
					else return 0;
				}
				else {
					if (aSort < bSort) {
						return 1;
					} 
					if (aSort > bSort) {
						return -1;
					}
					else return 0;
				}
		}
	}

});