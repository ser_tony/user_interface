  var lr = require('tiny-lr'), // Минивебсервер для livereload
    gulp = require('gulp'), // Сообственно Gulp JS
    jade = require('gulp-jade'), // Плагин для Jade
    stylus = require('gulp-stylus'), // Плагин для Stylus
    livereload = require('gulp-livereload'), // Livereload для Gulp
    myth = require('gulp-myth'), // Плагин для Myth - http://www.myth.io/
    csso = require('gulp-csso'), // Минификация CSS
    imagemin = require('gulp-imagemin'), // Минификация изображений
    uglify = require('gulp-uglify'), // Минификация JS
    concat = require('gulp-concat'), // Склейка файлов
    connect = require('connect'), // Webserver
    serveStatic = require('serve-static'),
    server = lr();




// Собираем JS
gulp.task('js', function() {
    gulp.src(['./js/*.js', './js/libs/**/*.js'])
        .pipe(gulp.dest('./public/js'))
        .pipe(livereload(server)); // даем команду на перезагрузку страницы
});


gulp.task('images', function() {
    gulp.src('./img/**/*')
        .pipe(gulp.dest('./public/img'))
});

gulp.task('css', function() {
    gulp.src('./css/**/*')
        .pipe(gulp.dest('./public/css'))
});

gulp.task('data', function() {
    gulp.src('./data/**/*')
        .pipe(gulp.dest('./public/data'))
});

gulp.task('index', function() {
    gulp.src('index.html')
        .pipe(gulp.src('./public'))
});



// Локальный сервер для разработки
gulp.task('http-server', function() {
    connect()
        //.use(require('connect-livereload')())
        .use(serveStatic('.'))
        .listen('8080');

    console.log('Server listening on http://localhost:8080');
});



// Запуск сервера разработки gulp watch
gulp.task('watch', function() {
    // Предварительная сборка проекта
    gulp.run('images');
    gulp.run('js');
    gulp.run('css');
    gulp.run('data');
    gulp.run('index');

    // Подключаем Livereload
    server.listen(35789, function(err) {
        if (err) return console.log(err);

        gulp.watch('img/**/*', function() {
            gulp.run('images');
        });
        gulp.watch('js/**/*', function() {
            gulp.run('js');
        });
        gulp.watch('css/**/*', function() {
            gulp.run('css');
        });
        gulp.watch('data/**/*', function() {
            gulp.run('data');
        });
        gulp.watch('index.html', function() {
            gulp.run('index');
        });

    });
    gulp.run('http-server');
});




 